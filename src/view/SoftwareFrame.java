package view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

public class SoftwareFrame extends JFrame{
	private static final double DEFAULT_RATE = 5;
	
	//1
	private JLabel showLabel;
	private JTextField showField;
	private JButton showButton;
	private JLabel showResultLabel;
	//2
	private JLabel showLabel2;
	private JTextField showField2; 
	private JButton showButton2;
	private JLabel showResultLabel2;
	
	public SoftwareFrame()
	{
		setSize(450, 110);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setLayout(null);
		
		showLabel = new JLabel("String : ");
		showLabel.setBounds(5,15,100,22);
	    
		showField = new JTextField();
		showField.setText("");	
		showField.setBounds(50,15,100,22);
	    
		showButton = new JButton("OK");
		showButton.setBounds(155,15,60,22);
	    
		showResultLabel = new JLabel("ASCII : " );
		showResultLabel.setBounds(35,40,500,22);
		
		getButton();
		
	    
		showLabel2 = new JLabel("Divide : ");
		showLabel2.setBounds(230,15,70,22);
	    
	    showField2 = new JTextField(10);
	    showField2.setText("");	
	    showField2.setBounds(280,15,72,22);
	    
	    showButton2 = new JButton("OK");
	    showButton2.setBounds(360,15,60,22);
	    
	    showResultLabel2 = new JLabel("remainder : " );
	    showResultLabel2.setBounds(230,40,160,22);
	    
	    add(this.showLabel);
	    add(this.showField);
	    add(this.showButton);
	    add(this.showResultLabel);  
	    add(this.showLabel2);
	    add(this.showField2);
	    add(this.showButton2);
	    add(this.showResultLabel2);  
	   
	    setVisible(true);
		setResizable(true);
	}
	
		
	public JTextField getField()
	{
		return showField;
	}
	public JButton getButton()
	{
		return showButton;
	}
	public void getResultLabel(int ascii)
	{
		showResultLabel.setText("ASCII : " + ascii);;
	}
	
	
	public JTextField getField2()
	{
		return showField2;
	}
	public JButton getButton2()
	{
		return showButton2;
	}
	public void getResultLabel2(int hash)
	{
		showResultLabel2.setText("remainder : " + hash);
	}
	

	
}



