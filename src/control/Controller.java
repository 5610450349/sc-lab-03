package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Hashing;
import view.SoftwareFrame;

public class Controller {

	private SoftwareFrame view;
	private Hashing model;
	private ActionListener actionListener1;
	private ActionListener actionListener2;
	

	
	public Controller(Hashing model, SoftwareFrame view)
	{
		this.model = model;
		this.view = view;
		
	}
	
	public void AddListener()
	{
		actionListener1 = new ActionListener() 
		{
			public void actionPerformed(ActionEvent actionEvent) {            
				int ascii = model.StringToASCII(view.getField().getText());
	            view.getResultLabel(ascii);
            }
		};   
		view.getButton().addActionListener(actionListener1); 
      
      actionListener2 = new ActionListener() 
      {
			public void actionPerformed(ActionEvent actionEvent) {            
				int n = Integer.parseInt(view.getField2().getText());
				int hash = model.division(n);
	            view.getResultLabel2(hash);
          }
      };   
      view.getButton2().addActionListener(actionListener2); 
	}
	
	public static void main(String[] args)
	{
		Hashing h = new Hashing();
		SoftwareFrame s = new SoftwareFrame();
		Controller control = new Controller(h,s);
		control.AddListener();
	}
	

 }


